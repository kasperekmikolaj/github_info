FROM bellsoft/liberica-openjdk-debian:17

RUN apt-get update \
    && apt-get install -y curl \
    && apt-get clean -y \
    && apt-get auto-remove -y \
    && rm -rf /var/lib/apt/lists/*

ARG USER=noname
ARG GROUP_ID=1000
ARG USER_ID=1000

RUN groupadd -g $GROUP_ID $USER \
    && useradd -u $USER_ID -g $GROUP_ID $USER \
    && mkdir -p /tmp/tomcat /static /logs \
    && chown -R $USER:$USER /tmp/tomcat /static /logs

HEALTHCHECK --interval=1m --timeout=6s --start-period=20s --retries=5 \
    CMD if curl -f ${APP_SCHEMA:-http}://localhost:${APP_PORT:-8080}/actuator/health | grep UP; then exit 0; else exit 1 ; fi

USER $USER

ARG JAR_FILE
COPY ${JAR_FILE} /app.jar

ENTRYPOINT ["java", "-jar", "/app.jar"]
