package pl.unityt.recruitment.exceptions;

import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
* Exception handler for application.
 * Used to avoid disclosure of implementation details.
*/
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InvalidRequestException.class)
    public String handleInvalidRequestException(RuntimeException exception) {
        log.error(exception.getMessage(), exception);
        return exception.getMessage();
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(RepositoryNotFoundException.class)
    public String handleRepositoryNotFoundException(RuntimeException exception) {
        log.error(exception.getMessage(), exception);
        return exception.getMessage();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(RuntimeException.class)
    public String handleRuntimeException(RuntimeException exception) {
        log.error(exception.getMessage(), exception);
        return "Unexpected behaviour";
    }
}
