package pl.unityt.recruitment.exceptions;

/**
* Exception used when request do not contain all required fields or some fields are not valid.
*/
public class InvalidRequestException extends RuntimeException {
    public InvalidRequestException(String message) {
        super(message);
    }
}
