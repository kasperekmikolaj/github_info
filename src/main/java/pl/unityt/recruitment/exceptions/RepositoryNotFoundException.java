package pl.unityt.recruitment.exceptions;

/**
 * Exception used when remote repository was not found.
 */
public class RepositoryNotFoundException extends RuntimeException {
    public RepositoryNotFoundException(String message, Exception exception) {
        super(message, exception);
    }
}
