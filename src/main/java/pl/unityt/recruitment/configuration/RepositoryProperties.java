package pl.unityt.recruitment.configuration;

import lombok.Getter;
import lombok.Setter;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
* Bean for getting list of properties from application.yml file under the 'repository' section.
*/
@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix = "repository")
public class RepositoryProperties {
    private String apiUrl;
}
