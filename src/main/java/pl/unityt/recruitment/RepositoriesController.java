package pl.unityt.recruitment;

import lombok.AllArgsConstructor;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import pl.unityt.recruitment.dto.RepositoryInfoResponseDto;
import pl.unityt.recruitment.services.RepositoryInfoService;

@RestController
@AllArgsConstructor
public class RepositoriesController {

    private final RepositoryInfoService repositoryInfoService;

    @GetMapping("/repositories/{owner}/{repository-name}")
    public RepositoryInfoResponseDto repositories(
            @PathVariable("owner") String owner, @PathVariable("repository-name") String repoName) {
        return repositoryInfoService.getRepositoryInformation(owner, repoName);
    }
}
