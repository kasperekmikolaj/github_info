package pl.unityt.recruitment.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Objects;

/** Class with basic information about remote repository used for message exchange using REST api. */
@Data
public class RepositoryInfoResponseDto {
    @JsonProperty("full_name")
    private String fullName;

    private String description;

    @JsonProperty("clone_url")
    private String cloneUrl;

    @JsonProperty("stargazers_count")
    private int stargazersCount;

    @JsonProperty("created_at")
    private LocalDateTime createdAt;

    public void setDescription(String newDescription) {
        description =
                Objects.isNull(newDescription) || newDescription.equals("null")
                        ? ""
                        : newDescription;
    }
}
