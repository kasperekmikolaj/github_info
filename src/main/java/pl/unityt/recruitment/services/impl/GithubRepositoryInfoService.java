package pl.unityt.recruitment.services.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import pl.unityt.recruitment.configuration.RepositoryProperties;
import pl.unityt.recruitment.dto.RepositoryInfoResponseDto;
import pl.unityt.recruitment.exceptions.InvalidRequestException;
import pl.unityt.recruitment.exceptions.RepositoryNotFoundException;
import pl.unityt.recruitment.services.RepositoryInfoService;

/** Implementation of {@link RepositoryInfoService} for Github repository. */
@Slf4j
@Service
@AllArgsConstructor
public class GithubRepositoryInfoService implements RepositoryInfoService {

    private final RestTemplate restTemplate;
    private final RepositoryProperties repositoryProperties;

    @Override
    public RepositoryInfoResponseDto getRepositoryInformation(String owner, String repoName) {
        if (owner.isBlank() || repoName.isBlank()) {
            throw new InvalidRequestException(
                    String.format(
                            "Invalid data passed in request, owner: %s and repository name: %s",
                            owner, repoName));
        }
        RepositoryInfoResponseDto repoInfo;
        try {
            repoInfo =
                    restTemplate.getForObject(
                            repositoryProperties.getApiUrl(),
                            RepositoryInfoResponseDto.class,
                            owner,
                            repoName);
        } catch (HttpClientErrorException.NotFound exception) {
            throw new RepositoryNotFoundException(
                    String.format(
                            "Repository info not found for owner: %s and repo: %s",
                            owner, repoName),
                    exception);
        }
        return repoInfo;
    }
}
