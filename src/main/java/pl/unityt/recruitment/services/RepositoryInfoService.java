package pl.unityt.recruitment.services;

import pl.unityt.recruitment.dto.RepositoryInfoResponseDto;

/** Service for retrieving information about specified code repository. */
public interface RepositoryInfoService {
/**
* Method for fetching basic repository information.
 * @param owner - owner of the repository
 * @param repoName - name of the repository
 * @return - {@link RepositoryInfoResponseDto} with basic information about repository.
*/
    RepositoryInfoResponseDto getRepositoryInformation(String owner, String repoName);
}
