package pl.unityt.recruitment.services;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import pl.unityt.recruitment.builders.RepositoryInfoResponseDtoBuilder;
import pl.unityt.recruitment.configuration.RepositoryProperties;
import pl.unityt.recruitment.dto.RepositoryInfoResponseDto;
import pl.unityt.recruitment.exceptions.InvalidRequestException;
import pl.unityt.recruitment.exceptions.RepositoryNotFoundException;

@SpringBootTest
class RepositoryInfoServiceTest {

    private static final String DEF_OWNER = "sampleOwner";
    private static final String DEF_REPO_NAME = "sampleRepo";

    @Autowired private RestTemplate restTemplate;

    @Autowired private RepositoryInfoService repositoryInfoService;
    @Autowired private RepositoryProperties repositoryProperties;

    private MockRestServiceServer mockServer;
    private String expectedUrl;
    private ObjectMapper mapper;

    @BeforeEach
    void init() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        expectedUrl =
                UriComponentsBuilder.fromUriString(repositoryProperties.getApiUrl())
                        .buildAndExpand(DEF_OWNER, DEF_REPO_NAME)
                        .toUriString();
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
    }

    @ParameterizedTest
    // given
    @CsvSource({"'', something", "something, ''"})
    void shouldThrowInvalidRequestWhenBlankValuesPassed(String owner, String repo) {
        // when then
        assertThatThrownBy(() -> repositoryInfoService.getRepositoryInformation(owner, repo))
                .isInstanceOf(InvalidRequestException.class);
    }

    @Test
    void shouldReturnCorrectRepositoryInfo() throws JsonProcessingException {
        // given
        RepositoryInfoResponseDto expectedResponse =
                RepositoryInfoResponseDtoBuilder.create().build();
        mockServer
                .expect(requestTo(expectedUrl))
                .andExpect(method(HttpMethod.GET))
                .andRespond(
                        withSuccess(
                                mapper.writeValueAsString(expectedResponse),
                                MediaType.APPLICATION_JSON));

        // when
        RepositoryInfoResponseDto actualResponse =
                repositoryInfoService.getRepositoryInformation(DEF_OWNER, DEF_REPO_NAME);

        // then
        mockServer.verify();
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    @Test
    void shouldThrowRepositoryNotFoundExceptionWhenNoRepositoryWasFound() {
        // given
        mockServer
                .expect(requestTo(expectedUrl))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withResourceNotFound());
        // when then
        assertThatThrownBy(() -> repositoryInfoService.getRepositoryInformation(DEF_OWNER, DEF_REPO_NAME))
                .isInstanceOf(RepositoryNotFoundException.class);
    }
}
