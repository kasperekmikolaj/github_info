package pl.unityt.recruitment.builders;

import pl.unityt.recruitment.dto.RepositoryInfoResponseDto;

import java.time.LocalDateTime;

public class RepositoryInfoResponseDtoBuilder {

    private static final String DEFAULT_CLONE_URL = "someUrl.com";
    private static final String DEFAULT_DESCRIPTION = "";
    private static final LocalDateTime DEFAULT_CREATED_AT = LocalDateTime.of(2022, 11,11,11,11);
    private static final String DEFAULT_FULL_NAME = "full repo name";
    private static final int DEFAULT_STARGAZERS_COUNT = 22;

    private final RepositoryInfoResponseDto dto;

    private RepositoryInfoResponseDtoBuilder() {
        dto = new RepositoryInfoResponseDto();
        dto.setCloneUrl(DEFAULT_CLONE_URL);
        dto.setDescription(DEFAULT_DESCRIPTION);
        dto.setCreatedAt(DEFAULT_CREATED_AT);
        dto.setFullName(DEFAULT_FULL_NAME);
        dto.setStargazersCount(DEFAULT_STARGAZERS_COUNT);
    }

    public static RepositoryInfoResponseDtoBuilder create() {
        return new RepositoryInfoResponseDtoBuilder();
    }

    public RepositoryInfoResponseDtoBuilder withFullName(String fullName) {
        dto.setFullName(fullName);
        return this;
    }

    public RepositoryInfoResponseDtoBuilder withDescription(String description) {
        dto.setDescription(description);
        return this;
    }

    public RepositoryInfoResponseDtoBuilder withCloneUrl(String cloneUrl) {
        dto.setCloneUrl(cloneUrl);
        return this;
    }

    public RepositoryInfoResponseDtoBuilder withStargazersCount(int stargazersCount) {
        dto.setStargazersCount(stargazersCount);
        return this;
    }

    public RepositoryInfoResponseDtoBuilder withCreatedAt(LocalDateTime createdAt) {
        dto.setCreatedAt(createdAt);
        return this;
    }

    public RepositoryInfoResponseDto build() {
        return dto;
    }
}