# Rekrutacja Unity-t

To jest szablon zadania rekrutacyjnego Unity-t.

Zadanie można rozwiązać w języku Java. Do konfiguracji i kompilacji projektu używamy Maven.

Prosimy o zastosowanie się do poniższych instrukcji:

1. Stwórz **prywatny** fork tego repozytorium https://bitbucket.org/unityttech/helikon/fork
2. Utwórz nową gałąź `solution` i użyj jej dodając swoje zmiany.
3. Utwórz pull request z gałęzi `solution` utworzonej w punkcie 2. do gałęzi `master` z **TWOJEGO REPOZYTORIUM** a
   nie **unityttech/helikon**.
4. Dodaj nam dostęp do odczytu do Twojego repozytorium. Szczegółowe informacje znajdziesz w pliku PDF, który od nas
   otrzymałeś.

> **UWAGA** Na gałęzi "master" nie powinno być żadnych Twoich zmian.

> Wszelkie niestosowanie się do instrukcji może skutować wykluczeniem z procesu rekrutacji.

# Running application

1. go into project directory
2. run in terminal:

```bash
docker-compose up -d
```

To stop the application run

```bash
docker-compose down
```
